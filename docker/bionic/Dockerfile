FROM ubuntu:18.04

ARG LLVM_URL='https://releases.llvm.org/3.8.1/clang+llvm-3.8.1-x86_64-linux-gnu-ubuntu-16.04.tar.xz'

RUN apt-get -qq update && \
    apt-get -qq install -y \
      build-essential \
      gcc-multilib \
      g++-multilib \
      libstdc++6 \
      libpython-dev \
      python-pip \
      python-virtualenv \
      python-setuptools \
      unzip \
      wget \
      lsb-release \
    && rm -rf /var/lib/apt/lists/*

RUN virtualenv -p python2 /root/venv_qsym && \
    /root/venv_qsym/bin/pip install -U pip && \
    /root/venv_qsym/bin/pip install -U six wheel setuptools twine && \
    /root/venv_qsym/bin/pip install -U pytest-xdist && \
    wget -qO /tmp/z3x86.zip 'https://github.com/Z3Prover/z3/releases/download/z3-4.6.0/z3-4.6.0-x86-ubuntu-14.04.zip' && \
    unzip -jd /usr/lib32 /tmp/z3x86.zip "*/bin/libz3.so" && \
    wget -qO /tmp/z3x64.zip 'https://github.com/Z3Prover/z3/releases/download/z3-4.6.0/z3-4.6.0-x64-ubuntu-16.04.zip' && \
    unzip -jd /usr/include /tmp/z3x64.zip "*/include/*.h" && \
    unzip -jd /usr/lib /tmp/z3x64.zip "*/bin/libz3.so" && \
    unzip -jd /usr/bin /tmp/z3x64.zip "*/bin/z3" && \
    wget -qO /tmp/llvm.tar.xz ${LLVM_URL} && \
    tar -C /usr/local --strip-components=1 -xf /tmp/llvm.tar.xz && \
    rm -rf /tmp/* && \
    ldconfig
