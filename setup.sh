#!/bin/bash

# check ptrace_scope for PIN
if ! grep -qF "0" /proc/sys/kernel/yama/ptrace_scope; then
  echo "Please run 'echo 0|sudo tee /proc/sys/kernel/yama/ptrace_scope'"
fi

git submodule init
git submodule update

install_z3_trusty () {
    wget -qO /tmp/z3x64.zip https://github.com/Z3Prover/z3/releases/download/z3-4.6.0/z3-4.6.0-x64-ubuntu-14.04.zip
    wget -qO /tmp/z3x86.zip https://github.com/Z3Prover/z3/releases/download/z3-4.6.0/z3-4.6.0-x86-ubuntu-14.04.zip
    sudo unzip -jd /usr/lib32 /tmp/z3x86.zip "*/bin/libz3.so"
    sudo unzip -jd /usr/include /tmp/z3x64.zip "*/include/*.h"
    sudo unzip -jd /usr/lib /tmp/z3x64.zip "*/bin/libz3.so"
    sudo unzip -jd /usr/bin /tmp/z3x64.zip "*/bin/z3"
    sudo ldconfig
}


install_z3_xenial() {
    wget -qO /tmp/z3x64.zip https://github.com/Z3Prover/z3/releases/download/z3-4.6.0/z3-4.6.0-x64-ubuntu-16.04.zip
    sudo unzip -jd /usr/include /tmp/z3x64.zip "*/include/*.h"
    sudo unzip -jd /usr/lib /tmp/z3x64.zip "*/bin/libz3.so"
    sudo unzip -jd /usr/bin /tmp/z3x64.zip "*/bin/z3"
    sudo ldconfig
}

install_z3_source() {
    pushd third_party/z3
    ./configure
    pushd build
    make -j$(nproc)
    sudo make install || make install
    popd
    rm -rf build
    ./configure --x86
    cd build
    make -j$(nproc)
    sudo cp libz3.so /usr/lib32/ || cp libz3.so /usr/lib32/
    popd
}


# install system deps
sudo apt-get update
sudo apt-get install -y libc6 libstdc++6 linux-libc-dev gcc-multilib \
  llvm-dev g++ g++-multilib python python-pip unzip \
  lsb-release

install_z3_source

# build test directories
pushd tests
python build.py
popd

cat <<EOM
Please install qsym by using (or check README.md):

  $ virtualenv venv
  $ source venv/bin/activate
  $ pip install .
EOM
