import tempfile
import os
import time
import shutil

import qsym


def check_stat_file(fp):
    data = dict()
    with open(fp) as fd:
        lines = fd.readlines()
    for line in lines:
        k, v = map(str.strip, line.split(':', 1))
        data[k] = v.strip(" %")
    if data['execs_done'] != '190':
        return False
    if data['cycles_done'] != '4':
        return False
    if data['exec_timeout'] != '5000':
        return False
    return True


def write_stats():
    output_dir = tempfile.mkdtemp(prefix="qsym-")
    name = "ls"
    afl = "ls0000"
    cmd = ['ls', '-la', '@@']
    timeout = 2000
    try:
        s = qsym.Stats(
                name=name,
                afl=afl,
                cmd=cmd,
                out_dir=output_dir,
                timeout=timeout)
        time.sleep(1.1)
        for i in xrange(20):
            s.execs_done += i
            s.add_path()
            if i % 2 == 0:
                s.add_hang()
            else:
                s.add_crash()
            if i % 5 == 0:
                s.cycles_done += 1
            s.paths_total += (i * i)
            s.paths_imported += 1
            s.update(5000)

        s.write()
        filepath = os.path.join(output_dir, 'fuzzer_stats')
        return check_stat_file(filepath)
    finally:
        shutil.rmtree(output_dir)


def test_stats():
    assert write_stats()
