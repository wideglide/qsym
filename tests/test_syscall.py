import logging
import os

from test_utils import get_testcases, run_single_test, TESTS_DIR


def pytest_generate_tests(metafunc):
    if 'target' in metafunc.fixturenames:
        testcases = get_testcases(os.path.join(TESTS_DIR, "syscall"))
        metafunc.parametrize('target', testcases)


def test_syscall(target):
    logging.getLogger('qsym.Executor').setLevel(logging.DEBUG)
    assert run_single_test(target)
